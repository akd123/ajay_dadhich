

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class TestNgAssignment2 {
	
    @BeforeSuite
 	public void setupSuite () {
        System.out.println("@BeforeSuite started.");
    }
 
    @BeforeTest
    public void setupTests () {
        System.out.println("@BeforeTest started.");
    }
    @BeforeClass
    public void setupClass () {
        System.out.println("@BeforeClass started.");
    }
 
    @BeforeMethod
    public void setupTest (){
        System.out.println("@BeforeMethod has started.");
    }
 
    @Test (groups = {"Regression", "Smoke"})
    public void firstTest () {
        System.out.println("1st Test is Started.");
    }
 
    @Test (groups = {"Regression", "Smoke"})
    public void secondTest () {
        System.out.println("2nd Test is Started.");
    }
 
    @Test (groups = {"Regression"})
    public void thirdTest () {
        System.out.println("3rd Test is Started.");
    }
 
    @Test (groups = {"Medium"})
    public void fourthTest () {System.out.println("4th Test is Started.");}
 
    @Test (groups = {"Regression"})
    public void fifthTest () {
        System.out.println("5th Test is Started.");
    }
 
    @Test (groups = {"Medium"})
    public void sixthTest () {
        System.out.println("6th Test is Started.");
    }
}