import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class ParameterizedTest1 {
	
	   @Test
	   @Parameters({"Name1", "Name2", "Name3", "Name4"})
	   public void parameterTest(String Name1, String Name2, String Name3, String Name4) {
	      System.out.println("My Name is : " + Name1);
	      System.out.println("My Name is : " + Name2);
	      System.out.println("My Name is : " + Name3);
	      System.out.println("My Name is : " + Name4);
	   }

}
